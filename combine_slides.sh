#!/bin/bash

rm -rf slides
mkdir -p slides/single slides/combined

pdftoppm -png "${1:?Pass pdf file to split as a first arg}" slides/single/slide

# List all slide images in order
slides=($(ls slides/single/slide-*.png | sort))

# Combine two slides at a time
for ((i=0; i<${#slides[@]}; i+=2)); do
    # Get slide paths
    slide1=${slides[i]}
    slide2=${slides[i+1]}

    # If the second slide doesn't exist, just copy the first slide
    if [[ -z $slide2 ]]; then
        cp "$slide1" "slides/combined/$(printf "combined-%03d.png" $((i/2+1)))"
    else
        convert "$slide1" "$slide2" -append "slides/combined/$(printf "combined-%03d.png" $((i/2+1)))"
    fi
done

echo "Combined images saved in the 'slides/combined' directory."

